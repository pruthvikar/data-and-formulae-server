import io
from django.db import models
from wand.image import Image as wandFile
from os import path
from datetime import datetime
from django.utils.timezone import utc
from equation.latex2png import Latex2Png
from django.core.exceptions import ValidationError
from django.core.files.base import File
import os
from sympy import latex
from tempfile import TemporaryFile
# Create your models here.
class Ack(models.Model):
    text=models.CharField(max_length=5000)
    class Meta:
        verbose_name_plural = 'Acknowledgements'


class Formula(models.Model):
    name=models.CharField(max_length=500)
    category=models.CharField(max_length=500)
    summary=models.CharField(max_length=1500)
    modified=models.DateTimeField(auto_now=True)
    sympy_equation=models.CharField(max_length=3000,blank=True)
    sympy_vars=models.CharField(max_length=500,blank=True)
    tags=models.CharField(max_length=2500,blank=True)
    legal=models.CharField(max_length=2500,blank=True)
    symbols=models.ManyToManyField("Symbol",blank=True)
    

    def getVariables(self):
        temp=[]
        for f in self.symbols.all():
            temp.append(f.sympy)
        return temp


    def __init__(self, *args, **kwargs):
      super(Formula, self).__init__(*args, **kwargs)
      
    def syncDict(self):
        sym=[]
        for s in self.symbols.all():
            sym.append(s.pk)   
        im=[]
        for i in self.images.all():
            im.append(i.pk)   
        sd={
            'pk':self.pk,
            'name':self.name,
            'category':self.category,
            'summary':self.summary,
            'legal':self.legal,
            'symbols':sym,
            'images':im
        }

        return sd
    
    def save(self, *args, **kwargs):
            self.tags = ' '.join([self.category,self.name,self.summary])
            super(Formula, self).save(*args, **kwargs)
            
    def __unicode__(self):
        return self.name
        
class Image(models.Model):
    img=models.FileField(upload_to='images/',blank=True)
    latex=models.CharField(max_length=1000,blank=True)
    modified=models.DateTimeField(auto_now=True)
    main=models.BooleanField(default=False)
    formula=models.ForeignKey('Formula',related_name='images')

    __original_latex = None

    def __init__(self, *args, **kwargs):
        super(Image, self).__init__(*args, **kwargs)
        self.__original_latex = self.latex

    def clean(self, *args, **kwargs):
        if not self.img:
            return
        filename = self.img.name
        ext = os.path.splitext(filename)[1]
        ext = ext.lower()
        if ext not in ['.pdf','.png','.jpg','.jpeg']:
            raise ValidationError('File must be a pdf or png')
        if ext in ['.pdf','.jpg','.jpeg'] :
            tpath='.'.join(filename.split('.')[:-1])+'.png'
            buffer = TemporaryFile()
            with wandFile(file=self.img, resolution=264) as img:
                img.trim(color=None, fuzz=0)
                img.save(file=buffer)
                buffer.seek(0)
                self.img=File(buffer)      
                self.img.name=tpath
    def save(self, *args, **kwargs):
          
        if (self.__original_latex != self.latex and self.latex) or not self.img:
            self.img=Latex2Png(self.latex)
        super(Image, self).save(*args, **kwargs)
        self.__original_latex = self.latex

    def syncDict(self):
        sd={
           'pk':self.pk,
           'image':self.img.url[1:],
           'main':self.main
        }
        return sd

    def __unicode__(self):
        return self.formula.name

class Symbol(models.Model):
    name=models.CharField(max_length=50)
    sympy=models.CharField(max_length=300)
    display=models.CharField(max_length=50)
    unitCategory=models.ForeignKey('UnitCategory')
    onlyReal=models.BooleanField(default=True)
    modified=models.DateTimeField(auto_now=True)
    constant=models.FloatField(default=0)
    isConstant=models.BooleanField(default=False)

    def syncDict(self):
        sd={'name':self.name,
        'onlyReal':self.onlyReal,
        'unit':self.unitCategory.pk,
        'pk':self.pk,
        'sympy':self.sympy,
        'isConstant':self.isConstant,
        'constant':self.constant,
        'display':self.display
        }
        return sd

    def __unicode__(self):
        if self.onlyReal:
            return self.name+' - '+self.sympy+' '+self.unitCategory.name
        return self.name+' - '+self.sympy+' '+self.unitCategory.name+' imaginary'


class UnitCategory(models.Model):
    name=models.CharField(max_length=100)
    isDimensionless=models.BooleanField(default=False)
    modified=models.DateTimeField(auto_now=True)

    def syncDict(self):
        unitSet=[]
        for u in self.units_set.all():
            unitSet.append(u.pk)
        sd={'name':self.name,
        'units':unitSet,
        'isDimensionless':self.isDimensionless,
        'pk':self.pk
        }
        return sd

    def __unicode__(self):
        return self.name    

    class Meta:
        verbose_name_plural = 'Unit Categories'


class Unit(models.Model):
    name=models.CharField(max_length=200)
    units=models.CharField(max_length=50)
    multiplicand=models.FloatField(default=1)
    addend=models.FloatField(default=0)
    isBaseUnit=models.BooleanField()
    category=models.ForeignKey('UnitCategory',related_name='units_set')
    modified=models.DateTimeField(auto_now=True)

    def syncDict(self):
        sd={'name':self.name,
        'units':self.units,
        'multiplicand':self.multiplicand,
        'addend':self.addend,
        'isBaseUnit':self.isBaseUnit,
        'pk':self.pk,
        }
        return sd


    def __unicode__(self):
        return self.name


class FeedBack(models.Model):
    summary=models.CharField(max_length=5000)
    
    def __unicode__(self):
        return self.summary

    class Meta:
        verbose_name_plural = 'Feedback'
