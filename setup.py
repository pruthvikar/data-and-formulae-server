from distutils.core import setup
                                                     
setup(  
	name = 'iapws',
	version = '1.0.2',
	py_modules = ['iapws'],
	author = 'jjgomera',
	author_email = 'jjgomera@gmail.com',
	url = '',
	description  = 'Python implementation of international-standard IAPWS-IF97 steam tables ',
	license = "gpl v3")
                                                                                

