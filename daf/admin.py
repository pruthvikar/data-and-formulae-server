from equation.models import Formula,UnitCategory,Unit,Symbol,Image,FeedBack, Ack
from django.conf import settings
from django.contrib import admin
from django import forms
from django.contrib.admin.widgets import AdminFileWidget
from django.utils.translation import ugettext as _
from django.utils.safestring import mark_safe
from django.forms import TextInput, Textarea,CheckboxInput,NumberInput
from django.db import models

class AdminImageWidget(AdminFileWidget):
    def render(self, name, value, attrs=None):
        output = []
        if value and getattr(value, "url", None):
            image_url = value.url
            file_name=str(value)
            output.append(u' <a href="%s" target="_blank"><img src="%s" alt="%s" /></a>' % \
                (image_url, image_url, file_name))
        output.append(super(AdminFileWidget, self).render(name, value, attrs))
        return mark_safe(u''.join(output))

class UnitInline(admin.TabularInline):
    model = Unit
    formfield_overrides = {
        models.CharField: {'widget': TextInput(attrs={'style':'width:80px'})},
        models.FloatField: {'widget': NumberInput(attrs={'style':'width:70px'})}
    }


class ImageInline(admin.StackedInline):
    model = Image
    extra=1
    fields=('latex','main','img',)
    def formfield_for_dbfield(self, db_field, **kwargs):
        if db_field.name == 'img':
            request = kwargs.pop("request", None)
            kwargs['widget'] = AdminImageWidget
            return db_field.formfield(**kwargs)
        return super(ImageInline,self).formfield_for_dbfield(db_field, **kwargs)

class FormulaModelForm( forms.ModelForm ):
    summary = forms.CharField( widget=forms.Textarea )
    class Meta:
        model = Formula

class FormulaAdmin(admin.ModelAdmin):
    form = FormulaModelForm
    model = Formula
    list_display=['name','category']

    filter_horizontal = ('symbols',)
    inlines=[ImageInline]
    exclude=('sympy_vars','tags')
    fields=('name','category','summary','sympy_equation','symbols','legal')


class SymbolAdmin(admin.ModelAdmin):
    model=Symbol

class ImageAdmin(admin.ModelAdmin):
    model=Image
    fields=('latex','main','img')
    def formfield_for_dbfield(self, db_field, **kwargs):
        if db_field.name == 'img':
            request = kwargs.pop("request", None)
            kwargs['widget'] = AdminImageWidget
            return db_field.formfield(**kwargs)
        return super(ImageAdmin,self).formfield_for_dbfield(db_field, **kwargs)

class UnitAdmin(admin.ModelAdmin):
    model=Unit
    list_display=['multiplicand']

class FeedBackAdmin(admin.ModelAdmin):
    model=FeedBack
    readonly_fields=('summary',)

class AckAdmin(admin.ModelAdmin):
    model=Ack


class UnitCategoryAdmin(admin.ModelAdmin):
    inlines=[UnitInline]

admin.autodiscover()
admin.site.register(Formula,FormulaAdmin)
admin.site.register(UnitCategory,UnitCategoryAdmin)
admin.site.register(Symbol,SymbolAdmin)
admin.site.register(FeedBack,FeedBackAdmin)
admin.site.register(Ack,AckAdmin)

